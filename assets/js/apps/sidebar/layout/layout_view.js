define(["app",
        "tpl!apps/sidebar/layout/layout.tpl"],
       function(Mis, layoutTpl){
  Mis.module("SidebarApp.Layout.View", function(View, Mis, Backbone, Marionette, $, _){
    View.Layout = Marionette.LayoutView.extend({
      
      template: layoutTpl,
    });


  });

  return Mis.SidebarApp.Layout.View;
});
