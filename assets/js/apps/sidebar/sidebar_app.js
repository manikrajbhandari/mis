define(["app","apps/dashboard/dashboard_app"], function(Mis, DashboardApp){
  Mis.module("SidebarApp", function(SidebarApp, ContactManager, Backbone, Marionette, $, _){
    //ContactsApp.startWithParent = false;

    SidebarApp.onStart = function(){
      console.log("starting SidebarApp");
    };

    SidebarApp.onStop = function(){
      console.log("stopping SidebarApp");
    };
  });

  return DashboardApp.SidebarApp;
});
