define(["app"], function(Mis){
  Mis.module("DashboardApp", function(DashboardApp, Mis, Backbone, Marionette, $, _){
    DashboardApp.startWithParent = false;

    DashboardApp.onStart = function(){
      // console.log("closing LoginApp");
      // require(["apps/login/login_app"], function () {
          
      //     Mis.LoginApp.stop();
      //   });
      console.log("starting DashboardApp");
    };

    DashboardApp.onStop = function(){
      console.log("stopping DashboardApp");
    };
  });

  return Mis.DashboardApp;
});
