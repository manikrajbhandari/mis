define(["marionette", "jquery-ui"], function(Marionette){
  var Mis = new Marionette.Application();

  Mis.navigate = function(route,  options){
    options || (options = {});
    Backbone.history.navigate(route, options);
  };

  Mis.getCurrentRoute = function(){
    return Backbone.history.fragment
  };

  Mis.startSubApp = function(appName, args){
    var currentApp = appName ? Mis.module(appName) : null;
    if (Mis.currentApp === currentApp){ return; }

    if (Mis.currentApp){
      Mis.currentApp.stop();
    }

    Mis.currentApp = currentApp;
    if(currentApp){
      currentApp.start(args);
    }
  };

  Mis.on("before:start", function(){
    var RegionContainer = Marionette.LayoutView.extend({
      el: "#app-container",

      regions: {
        //header: "#header-region",
        // sidebar: "#sidebar-region",
        main: "#main-region",
        dialog: "#dialog-region"
      }
    });

    Mis.regions = new RegionContainer();
    Mis.regions.dialog.onShow = function(view){
      var self = this;
      var closeDialog = function(){
        self.stopListening();
        self.empty();
        self.$el.dialog("destroy");
      };

      this.listenTo(view, "dialog:close", closeDialog);

      this.$el.dialog({
        modal: true,
        title: view.title,
        width: "auto",
        close: function(e, ui){
          closeDialog();
        }
      });
    };
  });

  Mis.on("start", function(){
    if(Backbone.history){
      // require(["apps/login/login_app"], function () {
      //   Backbone.history.start();

      //   if(Mis.getCurrentRoute() === ""){
      //     Mis.trigger("login:show");
      //   }
      // });
      require(["apps/dashboard/dashboard_app"], function () {
        Backbone.history.start();

        if(Mis.getCurrentRoute() === ""){
          Mis.trigger("dashboard:show");
        }
      });


    }
  });

  return Mis;
});
