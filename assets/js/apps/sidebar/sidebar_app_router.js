//dashboard app router
define(["app"], function(Mis){
  Mis.module("Routers.SidebarApp", function(SidebarAppRouter, Mis, Backbone, Marionette, $, _){
    SidebarAppRouter.Router = Marionette.AppRouter.extend({
      // appRoutes: {
      //   "dashboard": "showDashboard"
      // }
    });

    var executeAction = function(action, arg){
      Mis.startSubApp("SidebarApp");
      action(arg);
      // Mis.execute("set:active:header", "Dashboard");
      //Mis.execute("Dashboard");
    };

    var API = {
    showSidebar: function(){
        //Mis.LoginApp.stop();
        require(["apps/sidebar/layout/layout_controller"], function(LayoutController){
          Mis.startSubApp(null);
          executeAction(LayoutController.showLayout);
        });
      },

      // editContact: function(id){
      //   require(["apps/contacts/edit/edit_controller"], function(EditController){
      //     executeAction(EditController.editContact, id);
      //   });
      // }
    };

  	Mis.on("sidebar:show", function(){
        //Mis.navigate("dashboard");
        API.showDashboard();
      });

    Mis.Routers.on("start", function(){
      new SidebarAppRouter.Router({
        controller: API
      });
    });
  });

  return Mis.SidebarAppRouter;
});
