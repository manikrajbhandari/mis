//login app router
define(["app"], function(Mis){
  Mis.module("Routers.LoginApp", function(LoginAppRouter, Mis, Backbone, Marionette, $, _){
    LoginAppRouter.Router = Marionette.AppRouter.extend({
      appRoutes: {
        "login": "showLogin"
        // "contacts/:id": "showContact",
        // "contacts/:id/edit": "editContact"
      }
    });

    var executeAction = function(action, arg){
      Mis.startSubApp("LoginApp");
      action(arg);
      // Mis.execute("set:active:header", "login");
      //Mis.execute("login");
    };

    var API = {
      // listContacts: function(criterion){
      //   require(["apps/contacts/list/list_controller"], function(ListController){
      //     executeAction(ListController.listContacts, criterion);
      //   });
      // },

      // showContact: function(id){
      //   require(["apps/contacts/show/show_controller"], function(ShowController){
      //     executeAction(ShowController.showContact, id);
      //   });
      // },

      showLogin: function(){
        require(["apps/login/show/show_controller"], function(ShowController){
          executeAction(ShowController.showLogin);
        });
      },

      // editContact: function(id){
      //   require(["apps/contacts/edit/edit_controller"], function(EditController){
      //     executeAction(EditController.editContact, id);
      //   });
      // }
    };

    // LoginManager.on("contacts:list", function(){
    //   LoginManager.navigate("contacts");
    //   API.listContacts();
    // });

    // LoginManager.on("contacts:filter", function(criterion){
    //   if(criterion){
    //     LoginManager.navigate("contacts/filter/criterion:" + criterion);
    //   }
    //   else{
    //     LoginManager.navigate("contacts");
    //   }
    // });

    // LoginManager.on("contact:show", function(id){
    //   LoginManager.navigate("contacts/" + id);
    //   API.showContact(id);
    // });

	Mis.on("login:show", function(){
      Mis.navigate("login");
      API.showLogin();
    });

    // LoginManager.on("contact:edit", function(id){
    //   LoginManager.navigate("contacts/" + id + "/edit");
    //   API.editContact(id);
    // });

    Mis.Routers.on("start", function(){
      new LoginAppRouter.Router({
        controller: API
      });
    });
  });

  return Mis.LoginAppRouter;
});
