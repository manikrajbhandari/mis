<!-- Square card -->
  <div class="demo-ribbon"></div>
  <div class="demo-container mdl-grid">
            <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet">
            </div>
            <div class="mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet">
              <div class="login-card mdl-card mdl-shadow--4dp">
                <div class="mdl-card__title mdl-card--expand">
                  <h2 class="mdl-card__title-text">Login</h2>
                </div>
                <div class="mdl-card__supporting-text">
                  <!-- Textfield with Floating Label -->
                  <form action="#">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                      <input class="mdl-textfield__input" type="text" id="user">
                      <label class="mdl-textfield__label" for="username">Username</label>
                    </div>
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                      <input class="mdl-textfield__input" type="password" id="password">
                      <label class="mdl-textfield__label" for="password">Password</label>
                    </div>
                  </form>

                </div>
                <div class="mdl-card__actions mdl-card--border">
                  <a id="login" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                    Login
                  </a>
                  <a id="signup" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                    Sign Up
                  </a>
                </div>
              </div>

            </div>
        </div>

 
 




