define(["app",
        "tpl!apps/dashboard/layout/layout.tpl"],
       function(Mis, layoutTpl){
  Mis.module("DashboardApp.Layout.View", function(View, Mis, Backbone, Marionette, $, _){
    View.Layout = Marionette.LayoutView.extend({
      
      template: layoutTpl,

      regions: {
        headerRegion: "#header-region",
        sidebarRegion: "#sidebar-region",
        contentRegion: "#content-region"
      }
    });


  });

  return Mis.DashboardApp.Layout.View;
});
