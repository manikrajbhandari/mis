define(["app", "backbone.picky"], function(Mis){
  Mis.module("Entities", function(Entities, Mis, Backbone, Marionette, $, _){
    Entities.Sidebar = Backbone.Model.extend({
      initialize: function(){
        var selectable = new Backbone.Picky.Selectable(this);
        _.extend(this, selectable);
      }
    });

    Entities.SidebarCollection = Backbone.Collection.extend({
      model: Entities.Sidebar,

      initialize: function(){
        var singleSelect = new Backbone.Picky.SingleSelect(this);
        _.extend(this, singleSelect);
      }
    });

    var initializeSidebars = function(){
      Entities.Sidebars = new Entities.SidebarCollection([
        { name: "Attendence", url: "about", navigationTrigger: "about:show" },
        { name: "Result", url: "about", navigationTrigger: "about:show" }
      ]);
    };

    var API = {
      getSidebars: function(){
        if(Entities.Sidebars === undefined){
          initializeSidebars();
        }
        return Entities.Sidebars;
      }
    };

    Mis.reqres.setHandler("Sidebar:entities", function(){
      return API.getSidebars();
    });
  });

  return ;
});
