// define(["app", "apps/login/show/show_view"], function(LoginManager, View){
//   return {
//     showLogin: function(){
//       var view = new View.Layout();
//       Mis.regions.main.show(view);
//     }
//   };
// });
define(["app", "apps/login/show/show_view"], function(Mis, View){
  Mis.module("LoginApp.Show", function(Show, Mis, Backbone, Marionette, $, _){
    Show.Controller = {
      showLogin: function(){
      	var view = new View.Login();
        Mis.regions.main.show(view);

       var button = document.getElementsByClassName('mdl-button');
       _.each(button, function(element){
          componentHandler.upgradeElement(element);
      });
       var input = document.getElementsByClassName('mdl-textfield');
       console.log(input);
      _.each(input, function(element){
          componentHandler.upgradeElement(element);
      });
       
      }
    }
  });

  return Mis.LoginApp.Show.Controller;
});

