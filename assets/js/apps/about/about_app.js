define(["app", "marionette"], function(Mis, Marionette){
  var Router = Marionette.AppRouter.extend({
    appRoutes: {
      "about" : "showAbout"
    }
  });

  var API = {
    showAbout: function(){
      require(["apps/about/show/show_controller"], function(ShowController){
        Mis.startSubApp(null);
        ShowController.showAbout();
        Mis.execute("set:active:header", "about");
      });
    }
  };

  Mis.on("about:show", function(){
    Mis.navigate("about");
    API.showAbout();
  });

  Mis.on("before:start", function(){
    new Router({
      controller: API
    });
  });

  return Router;
});
