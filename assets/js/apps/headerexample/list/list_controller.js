define(["app", "apps/header/list/list_view"], function(Mis, View){
  Mis.module("HeaderApp.List", function(List, Mis, Backbone, Marionette, $, _){
    List.Controller = {
      listHeader: function(){
        require(["entities/header"], function(){
          var links = Mis.request("header:entities");
          var headers = new View.Headers({collection: links});

          headers.on("brand:clicked", function(){
            Mis.trigger("contacts:list");
          });

          headers.on("childview:navigate", function(childView, model){
            var trigger = model.get("navigationTrigger");
            Mis.trigger(trigger);
          });

          Mis.regions.header.show(headers);
        });
      },

      setActiveHeader: function(headerUrl){
        var links = Mis.request("header:entities");
        var headerToSelect = links.find(function(header){ return header.get("url") === headerUrl; });
        headerToSelect.select();
        links.trigger("reset");
      }
    };
  });

  return Mis.HeaderApp.List.Controller;
});
