define(["app"], function(Mis){
  Mis.module("LoginApp", function(LoginApp, Mis, Backbone, Marionette, $, _){
    LoginApp.startWithParent = false;

    LoginApp.onStart = function(){
      console.log("starting LoginApp");
    };

    LoginApp.onStop = function(){
      console.log("stopping LoginApp");
    };
  });

  return Mis.LoginApp;
});
