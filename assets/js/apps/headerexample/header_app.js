define(["app", "apps/header/list/list_controller"], function(Mis, ListController){
  Mis.module("HeaderApp", function(Header, Mis, Backbone, Marionette, $, _){
    Header.startWithParent = false;
    var API = {
      listHeader: function(){
        ListController.listHeader();
      }
    };

    Mis.commands.setHandler("set:active:header", function(name){
      ListController.setActiveHeader(name);
    });

    Header.on("start", function(){
      API.listHeader();
    });
  });

  return Mis.HeaderApp;
});
