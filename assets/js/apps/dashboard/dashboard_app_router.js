//dashboard app router
define(["app"], function(Mis){
  Mis.module("Routers.DashboardApp", function(DashboardAppRouter, Mis, Backbone, Marionette, $, _){
    DashboardAppRouter.Router = Marionette.AppRouter.extend({
      appRoutes: {
        "dashboard": "showDashboard"
      }
    });

    var executeAction = function(action, arg){
      Mis.startSubApp("DashboardApp");
      action(arg);
      // Mis.execute("set:active:header", "Dashboard");
      //Mis.execute("Dashboard");
    };

    var API = {
    showDashboard: function(){
        //Mis.LoginApp.stop();
        require(["apps/dashboard/layout/layout_controller"], function(LayoutController){
          Mis.startSubApp(null);
          executeAction(LayoutController.showLayout);
        });
      },

      // editContact: function(id){
      //   require(["apps/contacts/edit/edit_controller"], function(EditController){
      //     executeAction(EditController.editContact, id);
      //   });
      // }
    };

  	Mis.on("dashboard:show", function(){
        Mis.navigate("dashboard");
        API.showDashboard();
      });

    Mis.Routers.on("start", function(){
      new DashboardAppRouter.Router({
        controller: API
      });
    });
  });

  return Mis.DashboardAppRouter;
});
