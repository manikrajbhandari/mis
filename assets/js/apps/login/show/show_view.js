// define(["marionette", "tpl!apps/login/show/templates/layout.tpl"], function(Marionette, layoutTpl){
//   return {
//     Layout: Marionette.ItemView.extend({
//       template: layoutTpl
//     })
//   };
// });
define(["app",
         "tpl!apps/login/show/templates/layout.tpl"],
       function(Mis, layoutTpl){
  Mis.module("LoginApp.Show.View", function(View, Mis, Backbone, Marionette, $, _){
    // View.MissingContact = Marionette.ItemView.extend({
    //   template: missingTpl
    // });

    View.Login = Marionette.ItemView.extend({
      template: layoutTpl,

      events: {
        "click a#login": "loginClicked",
        "click a#signup": "signupClicked",
        'keydown': 'loginByEnter'
      },

      loginByEnter: function(evt){
         if (evt.keyCode == 13) {
          $("a#login").trigger("click");
          return false;
        }
      },

      loginClicked: function(e){
        e.preventDefault();
        alert("loginClicked");

        require(["apps/dashboard/dashboard_app"], function () {
          
          Mis.trigger("dashboard:show");
        });
        
        //this.trigger("contact:edit", this.model);
      },

       signupClicked: function(e){
        e.preventDefault();
        alert("signupClicked");
        //this.trigger("contact:edit", this.model);
      }

    });
  });

  return Mis.LoginApp.Show.View;
});
